# Horus AI Photo Warning System

AI Photo Warning System


Team - Horus
Members - Chandan, Talat, Aniket, Aishwarya


Summary: Existing systems take minimum 5 mins on average to approve uploaded photos in case of automated flow & extends further if assigned to manual queue. Also, the member lacks awareness if photos are rejected. We have introduced an AI photo warning system, which informs users proactively during the photo upload state about the likelihood of photos getting rejected & providing guidelines about photo upload rules.
