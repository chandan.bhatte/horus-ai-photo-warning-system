import json
import traceback
from flask import Flask, request
import boto3
import os
import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing import image
import io, logging
from PIL import Image
from flask_cors import CORS


app = Flask(__name__)
CORS(app)
logging.getLogger('flask_cors').level = logging.DEBUG


bucket_name = 'sangam-staging-photos-to-process'
# Create a session using your AWS credentials
session = boto3.Session(
)
    
model_path = '/home/chandanb/python/sgm_photo_efficientnet224'
model = tf.keras.models.load_model(model_path)
normalization_layer = tf.keras.layers.Rescaling(1. / 255)
preprocessing_model = tf.keras.Sequential([normalization_layer])

# Create an S3 client
s3 = session.client('s3')

@app.route('/photo/<memberlogin>/uploads', methods=['POST'])
def upload_files(memberlogin):
    op = []
    try:
        for file in request.files.getlist('files'):
            if file:
                filebytes = file.read()
                prediction = False
                prediction = getPrediction(filebytes)
                # Generate a unique file name
                filename = file.filename
                file_key = f'2023/hackfest/{memberlogin}-{filename}'

                # Upload the file to S3
                s3.upload_fileobj(
                    io.BytesIO(filebytes),
                    bucket_name,
                    file_key,
                    ExtraArgs={'ACL': 'public-read'}
                )
                file_url = f'https://sangam-staging-photos-to-process.s3.ap-south-1.amazonaws.com/{file_key}'
                op.append({"name":filename, "path":file_url,"warning":prediction,"status":"Success","type": "image/png","size":542309})
        return { "data" : op }, 200
    except Exception as e:
        traceback.print_exc()
        return str(e), 500
    
def getPrediction(filebytes):
    img = Image.open(io.BytesIO(filebytes))
    img = img.convert('RGB')
    img = img.resize((224, 224), Image.NEAREST)
    img_array = image.img_to_array(img)
    img_array = tf.image.convert_image_dtype(img_array, tf.float32)
    img_array = tf.expand_dims(img_array, axis=0)
    img_array = preprocessing_model(img_array)
    output = model(img_array)
    predicted_class = np.argmax(output)
    if predicted_class == 1: #show warning
        return True
    return False
if __name__ == '__main__':
    app.run()
